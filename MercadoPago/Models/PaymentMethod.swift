//
//  PaymentMethod.swift
//  MercadoPago
//
//  Created by Ximena Arriagada on 19-10-18.
//

import Foundation

class PaymentMethod {
    
    var id: String?
    var name: String?
    var type: String?
    var status: String?
    var imageURL: String?
    var minAllowedAmount: Double?
    var maxAllowedAmount: Double?
    
    init(id: String?, name: String?, type: String?, status: String?, imageURL: String?, minAllowedAmount: Double?, maxAllowedAmount: Double?) {
        self.id = id
        self.name = name
        self.type = type
        self.status = status
        self.imageURL = imageURL
        self.minAllowedAmount = minAllowedAmount
        self.maxAllowedAmount = maxAllowedAmount
    }
    
}
