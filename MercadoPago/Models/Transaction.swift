//
//  Transaction.swift
//  MercadoPago
//
//  Created by Ximena Arriagada on 19-10-18.
//

import Foundation

class Transaction {
    
    var amount: Double?
    var paymentMethod: PaymentMethod?
    var cardIssuer: CardIssuer?
    var installment: Installment?
    
    init(amount: Double? = nil, paymentMethod: PaymentMethod? = nil, cardIssuer: CardIssuer? = nil, installment: Installment? = nil) {
        self.amount = amount
        self.paymentMethod = paymentMethod
        self.cardIssuer = cardIssuer
        self.installment = installment
    }
    
}
