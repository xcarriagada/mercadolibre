//
//  CardIssuer.swift
//  MercadoPago
//
//  Created by Ximena Arriagada on 19-10-18.
//

import Foundation

class CardIssuer {
    
    var id: String?
    var name: String?
    var imageURL: String?
    
    init(id: String?, name: String?, imageURL: String?) {
        self.id = id
        self.name = name
        self.imageURL = imageURL
    }
    
}
