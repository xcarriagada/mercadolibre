//
//  Installment.swift
//  MercadoPago
//
//  Created by Ximena Arriagada on 19-10-18.
//

import Foundation

class Installment {
    
    var message: String?
    
    init(message: String?) {
        self.message = message
    }
    
}
