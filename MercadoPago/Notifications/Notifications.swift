//
//  Notifications.swift
//  MercadoPago
//
//  Created by Ximena Arriagada on 23-10-18.
//

import Foundation

extension Notification.Name {
    static let transaction = Notification.Name(rawValue: "Transaction")
}
