//
//  CreditCardsViewModel.swift
//  MercadoPago
//
//  Created by Ximena Arriagada on 20-10-18.
//

import Foundation

class CreditCardsViewModel {
    
    var creditCardViewModels = [CreditCardViewModel]()
    
    func build(onFailure fail: @escaping ((String?) -> ()), onSuccess success: @escaping ([CreditCardViewModel]) -> ()) {
        let dao = PaymentMethodDAO()
        dao.paymentMethods(onFailure: { (error) in
            fail(error!)
        }, onSuccess: { (paymentMethods) in
            if paymentMethods != nil {
                for paymentMethod in paymentMethods! {
                    if let type = paymentMethod.type, let status = paymentMethod.status {
                        if type == "credit_card" && status == "active" {
                            self.creditCardViewModels.append(CreditCardViewModel(paymentMethod: paymentMethod))
                        }
                    }
                }
            }
            success(self.creditCardViewModels)
        })
    }
    
}
