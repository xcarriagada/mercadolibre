//
//  CreditCardViewModel.swift
//  MercadoPago
//
//  Created by Ximena Arriagada on 20-10-18.
//

import Foundation

class CreditCardViewModel {
    
    var id: String?
    var name: String?
    var imageURL: String?
    var minAllowedAmount: Double?
    var maxAllowedAmount: Double?
    
    init(paymentMethod: PaymentMethod) {
        
        self.id = paymentMethod.id
        self.name = paymentMethod.name
        
        self.imageURL = paymentMethod.imageURL
        
        if let minAllowedAmount = paymentMethod.minAllowedAmount {
            if minAllowedAmount > 0 {
                self.minAllowedAmount = minAllowedAmount
            }
        }
        
        self.maxAllowedAmount = paymentMethod.maxAllowedAmount
        
    }
}
