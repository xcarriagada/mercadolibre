//
//  CardIssuerViewModels.swift
//  MercadoPago
//
//  Created by Ximena Arriagada on 20-10-18.
//

import Foundation

class CardIssuersViewModel {
    
    var cardIssuerViewModels = [CardIssuerViewModel]()
    
    func build(for paymentMethodId: String, onFailure fail: @escaping ((String?) -> ()), onSuccess success: @escaping ([CardIssuerViewModel]) -> ()) {
        let dao = CardIssuerDAO()
        dao.cardIssuers(for: paymentMethodId, onFailure: { (error) in
            fail(error!)
        }, onSuccess: { (cardIssuers) in
            if cardIssuers != nil {
                for cardIssuer in cardIssuers! {
                    self.cardIssuerViewModels.append(CardIssuerViewModel(cardIssuer: cardIssuer))
                }
            }
            success(self.cardIssuerViewModels)
        })
    }
    
}
