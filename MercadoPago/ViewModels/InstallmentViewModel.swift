//
//  InstallmentViewModel.swift
//  MercadoPago
//
//  Created by Ximena Arriagada on 20-10-18.
//

import Foundation

class InstallmentViewModel {
    
    var message: String?
    
    init(installment: Installment) {
        self.message = installment.message
    }
}
