//
//  CardIssuerViewModel.swift
//  MercadoPago
//
//  Created by Ximena Arriagada on 20-10-18.
//

import Foundation

class CardIssuerViewModel {
    
    var cardIssuer: CardIssuer?
    
    init(cardIssuer: CardIssuer) {
        self.cardIssuer = cardIssuer
    }
    
}
