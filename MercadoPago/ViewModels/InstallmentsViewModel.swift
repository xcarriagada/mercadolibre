//
//  InstallmentsViewModel.swift
//  MercadoPago
//
//  Created by Ximena Arriagada on 20-10-18.
//

import Foundation

class InstallmentsViewModel {
    
    var installmentViewModels = [InstallmentViewModel]()
    
    func build(for transaction: TransactionViewModel, onFailure fail: @escaping ((String?) -> ()), onSuccess success: @escaping ([InstallmentViewModel]) -> ()) {
        let dao = InstallmentDAO()
        dao.installments(for: transaction.amount!, transaction.creditCardViewModel!.id!, transaction.cardIssuerViewModel?.cardIssuer?.id ?? nil, onFailure: { (error) in
            fail(error!)
        }, onSuccess: { (installments) in
            if installments != nil {
                for installment in installments! {
                    if installment.message != nil {
                        self.installmentViewModels.append(InstallmentViewModel(installment: installment))
                    }
                }
            }
            success(self.installmentViewModels)
        })
    }
}
