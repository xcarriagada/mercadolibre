//
//  TransactionViewModel.swift
//  MercadoPago
//
//  Created by Ximena Arriagada on 20-10-18.
//

import Foundation

class TransactionViewModel {
    
    var amount: Double?
    var creditCardViewModel: CreditCardViewModel?
    var cardIssuerViewModel: CardIssuerViewModel?
    var installmentViewModel: InstallmentViewModel?
    
    init(amount: Double, creditCardViewModel: CreditCardViewModel? = nil, cardIssuerViewModel: CardIssuerViewModel? = nil, installmentViewModel: InstallmentViewModel? = nil) {
        
        self.amount = amount
        
        if creditCardViewModel != nil {
            self.creditCardViewModel = creditCardViewModel
        }
        
        if cardIssuerViewModel != nil {
            self.cardIssuerViewModel = cardIssuerViewModel
        }
        
        if installmentViewModel != nil {
            self.installmentViewModel = installmentViewModel
        }
    }
    
}
