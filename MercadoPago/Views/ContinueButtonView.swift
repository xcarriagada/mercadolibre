//
//  ContinueButtonView.swift
//  MercadoPago
//
//  Created by Ximena Arriagada on 20-10-18.
//

import UIKit

class ContinueButtonView: UIView {
    
    @IBOutlet weak var continueButton: UIButton!
    
}
