//
//  APIClient.swift
//  MercadoPago
//
//  Created by Ximena Arriagada on 19-10-18.
//

import Foundation
import Alamofire

typealias arrayDictionaryResponse = ([[String: AnyObject]]?) -> Void
typealias errorResponse = (String?) -> Void

class APIClient {
    
    static let shared = APIClient()
    
    let publicKey = "444a9ef5-8a6b-429f-abdf-587639155d88"
    
    let paymentMethodsURL = "https://api.mercadopago.com/v1/payment_methods"
    let carIssuersURL = "https://api.mercadopago.com/v1/payment_methods/card_issuers"
    let installmentsURL = "https://api.mercadopago.com/v1/payment_methods/installments"
    
    func getPaymentMethods(completionHandler: @escaping arrayDictionaryResponse, errorHandler: @escaping errorResponse){
        
        let params = ["public_key": publicKey]
        
        Alamofire.request(paymentMethodsURL, method: .get, parameters: params).responseJSON { [weak self] response in
            switch response.result {
                case .success:
                    if let dict = response.value as? [[String: AnyObject]] {
                        completionHandler(dict)
                    }
                case .failure:
                    errorHandler(self?.getMessageError(response: response.response))
            }
        }
        
    }
    
    func getCardIssuers(for paymentMethodId: String, completionHandler: @escaping arrayDictionaryResponse, errorHandler: @escaping errorResponse) {
        
        let params = ["public_key" : publicKey,"payment_method_id": paymentMethodId ]
        
        Alamofire.request(carIssuersURL, method: .get, parameters: params).responseJSON { [weak self] response in
            switch response.result {
            case .success:
                if let dict = response.value as? [[String: AnyObject]] {
                    completionHandler(dict)
                }
            case .failure:
                errorHandler(self?.getMessageError(response: response.response))
            }
        }
    }
    
    func getInstallments(for paymentMethodId: String,_ amount: Double, _ cardIssuerId: String? = nil, completionHandler: @escaping arrayDictionaryResponse, errorHandler: @escaping errorResponse) {
        
        var params: [String : Any] = [:]
        
        if cardIssuerId != nil {
            params = ["public_key" : publicKey,"payment_method_id": paymentMethodId, "amount": amount, "issuer.id": cardIssuerId!]
        } else {
            params = ["public_key" : publicKey,"payment_method_id": paymentMethodId, "amount": amount]
        }
        
        Alamofire.request(installmentsURL, method: .get, parameters: params).responseJSON { [weak self] response in
            switch response.result {
            case .success:
                if let dicts = response.value as? [[String: AnyObject]], let dict = dicts[0]["payer_costs"] as? [[String: AnyObject]] {
                    completionHandler(dict)
                }
            case .failure:
                errorHandler(self?.getMessageError(response: response.response))
            }
        }
    }
    
    func getMessageError(response: URLResponse?) -> String {
        
        if let httpResponse = response as? HTTPURLResponse {
            switch httpResponse.statusCode {
                case 401:
                    return "401 - NO AUTORIZADO"
                case 404:
                    return "404 - NO ENCONTRADO"
                default:
                    return "\(httpResponse.statusCode) - PETICIÓN INCORRECTA"
            }
        } else {
            return "ERROR INESPERADO"
        }
    }
}
