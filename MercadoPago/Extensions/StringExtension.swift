//
//  StringExtension.swift
//  MercadoPago
//
//  Created by Ximena Arriagada on 22-10-18.
//

import Foundation

extension String {
    
    func toDouble() -> Double? {
        return NumberFormatter().number(from: self)?.doubleValue
    }
    
}
