//
//  CreditCardViewController.swift
//  MercadoPago
//
//  Created by Ximena Arriagada on 20-10-18.
//

import UIKit

class CreditCardViewController: UIViewController, UITableViewDelegate {
    
    @IBOutlet weak var creditCardTableView: UITableView!
    
    var creditCardsViewModel: CreditCardsViewModel?
    var transactionViewModel: TransactionViewModel?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.creditCardsViewModel = CreditCardsViewModel()
        self.getAll()
        
        self.creditCardTableView.dataSource = self
        self.creditCardTableView.delegate = self
        
        self.creditCardTableView.register(UINib(nibName: "DetailTableViewCell", bundle: nil), forCellReuseIdentifier: "DetailTableViewCell")
        
        setupNavigationBar()
        
    }
    
    func setupNavigationBar() {
        self.navigationController?.navigationBar.barTintColor = UIColor(red: 22/255, green: 140/255, blue: 211/255, alpha: 1)
        self.navigationController?.navigationBar.titleTextAttributes = [NSAttributedString.Key.foregroundColor : UIColor.white]
        self.navigationItem.title = "Tarjeta de Crédito"
    }
    
    func getAll() {
        self.creditCardsViewModel?.build(onFailure: { (error) in
            print(error!)
        }, onSuccess: { (paymentMethodViewModels) in
            print(paymentMethodViewModels.count)
            DispatchQueue.main.async {
                self.creditCardTableView.reloadData()
            }
        })
    }
    
}
