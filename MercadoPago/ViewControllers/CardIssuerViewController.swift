//
//  CardIssuerViewController.swift
//  MercadoPago
//
//  Created by Ximena Arriagada on 20-10-18.
//

import UIKit

class CardIssuerViewController: UIViewController, UITableViewDelegate {
    
    @IBOutlet weak var cardIssuerTableView: UITableView!
    
    var cardIssuersViewModel: CardIssuersViewModel?
    var transactionViewModel: TransactionViewModel?
    
    required convenience init(viewModel: CardIssuersViewModel) {
        self.init(nibName: nil, bundle: nil)
        
        self.cardIssuersViewModel = viewModel

    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.cardIssuerTableView.delegate = self
        self.cardIssuerTableView.dataSource = self
        
        self.cardIssuerTableView.register(UINib(nibName: "DetailTableViewCell", bundle: nil), forCellReuseIdentifier: "DetailTableViewCell")
        
        setupNavigationBar()
    }

    func setupNavigationBar() {
        self.navigationController?.navigationBar.barTintColor = UIColor(red: 22/255, green: 140/255, blue: 211/255, alpha: 1)
        self.navigationController?.navigationBar.titleTextAttributes = [NSAttributedString.Key.foregroundColor : UIColor.white]
        self.navigationItem.title = "Banco"
    }
}
