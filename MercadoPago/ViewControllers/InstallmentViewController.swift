//
//  InstallmentViewController.swift
//  MercadoPago
//
//  Created by Ximena Arriagada on 20-10-18.
//

import UIKit

class InstallmentViewController: UIViewController, UITableViewDelegate {

    @IBOutlet weak var installmentTableView: UITableView!
    
    var installmentsViewModel: InstallmentsViewModel?
    var transactionViewModel: TransactionViewModel?
    
    required convenience init(viewModel: InstallmentsViewModel) {
        self.init(nibName: nil, bundle: nil)
        
        self.installmentsViewModel = viewModel
        
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.installmentTableView.delegate = self
        self.installmentTableView.dataSource = self
        
        self.installmentTableView.register(UINib(nibName: "DetailTableViewCell", bundle: nil), forCellReuseIdentifier: "DetailTableViewCell")
        
        setupNavigationBar()
    }

    func setupNavigationBar() {
        self.navigationController?.navigationBar.barTintColor = UIColor(red: 22/255, green: 140/255, blue: 211/255, alpha: 1)
        self.navigationController?.navigationBar.titleTextAttributes = [NSAttributedString.Key.foregroundColor : UIColor.white]
        self.navigationItem.title = "Cuotas"
    }
}
