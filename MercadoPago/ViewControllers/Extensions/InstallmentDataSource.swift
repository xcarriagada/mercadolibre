//
//  InstallmentDataSource.swift
//  MercadoPago
//
//  Created by Ximena Arriagada on 20-10-18.
//

import UIKit

extension InstallmentViewController: UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.installmentsViewModel!.installmentViewModels.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "DetailTableViewCell") as! DetailTableViewCell
        cell.loadInstallment(viewModel: self.installmentsViewModel!.installmentViewModels[indexPath.row])
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let destination = AmountViewController()
        self.transactionViewModel = TransactionViewModel(amount: self.transactionViewModel!.amount!, creditCardViewModel: self.transactionViewModel!.creditCardViewModel!, cardIssuerViewModel: self.transactionViewModel?.cardIssuerViewModel ?? nil, installmentViewModel: self.installmentsViewModel!.installmentViewModels[indexPath.row])
        self.navigationController?.pushViewController(destination, animated: true)
        NotificationCenter.default.post(name: Notification.Name.transaction, object: self.transactionViewModel)
    }
}
