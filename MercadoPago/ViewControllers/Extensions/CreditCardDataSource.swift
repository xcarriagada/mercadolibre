//
//  PaymentMethodDataSource.swift
//  MercadoPago
//
//  Created by Ximena Arriagada on 20-10-18.
//

import UIKit

extension CreditCardViewController: UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.creditCardsViewModel!.creditCardViewModels.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "DetailTableViewCell") as! DetailTableViewCell
        cell.loadCreditCard(viewModel: creditCardsViewModel!.creditCardViewModels[indexPath.row])
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let cardIssuersViewModel = CardIssuersViewModel()
        self.transactionViewModel = TransactionViewModel(amount: self.transactionViewModel!.amount!, creditCardViewModel: self.creditCardsViewModel!.creditCardViewModels[indexPath.row])
        cardIssuersViewModel.build(for: creditCardsViewModel!.creditCardViewModels[indexPath.row].id!, onFailure: { (error) in
            print(error!)
        }, onSuccess: { (cardIssuerViewModels) in
            if cardIssuerViewModels.count == 0 {
                let installmentsViewModel = InstallmentsViewModel()
                installmentsViewModel.build(for: self.transactionViewModel!, onFailure: { (error) in
                    print(error!)
                }, onSuccess: { (installmentViewModels) in
                    if installmentViewModels.count == 0 {
                        let destination = AmountViewController()
                        self.navigationController?.pushViewController(destination, animated: true)
                        NotificationCenter.default.post(name: Notification.Name.transaction, object: self.transactionViewModel)
                    } else {
                        let destination = InstallmentViewController(viewModel: installmentsViewModel)
                        destination.transactionViewModel = self.transactionViewModel
                        self.navigationController?.pushViewController(destination, animated: true)
                    }
                })
            } else {
                let destination = CardIssuerViewController(viewModel: cardIssuersViewModel)
                destination.transactionViewModel = self.transactionViewModel
                self.navigationController?.pushViewController(destination, animated: true)
            }
        })
    }
    
}
