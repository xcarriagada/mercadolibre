//
//  CardIssuerDataSource.swift
//  MercadoPago
//
//  Created by Ximena Arriagada on 20-10-18.
//

import UIKit

extension CardIssuerViewController: UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.cardIssuersViewModel!.cardIssuerViewModels.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "DetailTableViewCell") as! DetailTableViewCell
        cell.loadCardIssuer(viewModel: self.cardIssuersViewModel!.cardIssuerViewModels[indexPath.row])
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let installmentsViewModel = InstallmentsViewModel()
        self.transactionViewModel = TransactionViewModel(amount: self.transactionViewModel!.amount!, creditCardViewModel: self.transactionViewModel!.creditCardViewModel!, cardIssuerViewModel: self.cardIssuersViewModel!.cardIssuerViewModels[indexPath.row])
        installmentsViewModel.build(for: self.transactionViewModel!, onFailure: { (error) in
            print(error!)
        }, onSuccess: { (installmentViewModels) in
            if installmentViewModels.count == 0 {
                let destination = AmountViewController()
                self.navigationController?.pushViewController(destination, animated: true)
                NotificationCenter.default.post(name: Notification.Name.transaction, object: self.transactionViewModel)
            } else {
                let destination = InstallmentViewController(viewModel: installmentsViewModel)
                destination.transactionViewModel = self.transactionViewModel
                self.navigationController?.pushViewController(destination, animated: true)
            }
        })
        
    }
}
