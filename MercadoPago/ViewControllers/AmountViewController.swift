//
//  AmountViewController.swift
//  MercadoPago
//
//  Created by Ximena Arriagada on 20-10-18.
//

import UIKit

class AmountViewController: UIViewController, UITextFieldDelegate {

    @IBOutlet var amountView: UIView!
    @IBOutlet weak var amountTextField: UITextField!
    @IBOutlet weak var continueButton: UIButton!
    
    var transactionViewModel: TransactionViewModel?
    
    override func viewDidLoad() {
        super.viewDidLoad()

        setupNavigationBar()
        
        self.continueButton.layer.cornerRadius = 5
        
        NotificationCenter.default.addObserver(self, selector: #selector(showTransaction(_:)), name: Notification.Name.transaction, object: transactionViewModel)
        
    }
    
    func setupNavigationBar() {
        self.navigationController?.navigationBar.barTintColor = UIColor(red: 22/255, green: 140/255, blue: 211/255, alpha: 1)
        self.navigationController?.navigationBar.titleTextAttributes = [NSAttributedString.Key.foregroundColor : UIColor.white]
        self.navigationItem.title = "Pagar Servicios"
    }

    @IBAction func `continue`(_ sender: Any) {
        if let amountString = self.amountTextField.text, !amountString.isEmpty {
            let amount = amountString.toDouble()
            self.transactionViewModel = TransactionViewModel(amount: amount!)
            
            let destination = CreditCardViewController()
            destination.transactionViewModel = self.transactionViewModel
            self.navigationController?.pushViewController(destination, animated: true)
        } else {
            let alertController = UIAlertController(title: "Mercado Pago", message: "Ingresa el monto a pagar", preferredStyle: UIAlertController.Style.alert)
            alertController.addAction(UIAlertAction(title: "Aceptar", style: UIAlertAction.Style.default,handler: nil))
            
            self.present(alertController, animated: true, completion: nil)
        }
    }
    
    @objc func showTransaction(_ notification: Notification) {
        self.transactionViewModel = notification.object as? TransactionViewModel
        var message = "Pago de $\(self.transactionViewModel!.amount!)"
        
        if let creditCard = self.transactionViewModel!.creditCardViewModel?.name {
            message = "\(message), con \(creditCard)"
        }
        if let cardIssuer = self.transactionViewModel!.cardIssuerViewModel?.cardIssuer?.name {
            message = "\(message) de \(cardIssuer)"
        }
        if let installment = self.transactionViewModel!.installmentViewModel?.message {
            message = "\(message) en \(installment)"
        }
        
        let alertController = UIAlertController(title: "Mercado Pago", message: message, preferredStyle: UIAlertController.Style.alert)
        alertController.addAction(UIAlertAction(title: "Aceptar", style: UIAlertAction.Style.default,handler: nil))
        
        self.present(alertController, animated: true, completion: nil)
    }
    
}
