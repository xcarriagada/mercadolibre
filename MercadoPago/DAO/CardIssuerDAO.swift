//
//  CardIssuerDAO.swift
//  MercadoPago
//
//  Created by Ximena Arriagada on 19-10-18.
//

import Foundation

class CardIssuerDAO {
    
    func cardIssuers(for paymentMethodId: String, onFailure fail: @escaping ((String?) -> ()), onSuccess success: @escaping ([CardIssuer]?) -> ()) {
        let client = APIClient.shared
        client.getCardIssuers(for: paymentMethodId, completionHandler: { (cardIssuersDicts) in
            var cardIssuers = [CardIssuer]()
            if cardIssuersDicts != nil {
                for dict in cardIssuersDicts! {
                    let cardIssuer = CardIssuer(id: dict["id"] as? String, name: dict["name"] as? String, imageURL: dict["thumbnail"] as? String)
                    cardIssuers.append(cardIssuer)
                }
            }
            success(cardIssuers)
        }, errorHandler: { (error) in
            fail(error!)
        })
    }
    
}
