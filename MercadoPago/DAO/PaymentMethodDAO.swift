//
//  PaymentMethodDAO.swift
//  MercadoPago
//
//  Created by Ximena Arriagada on 19-10-18.
//

import Foundation

class PaymentMethodDAO {
    
    func paymentMethods(onFailure fail: @escaping ((String?) -> ()), onSuccess success: @escaping ([PaymentMethod]?) -> ()) {
        let client = APIClient.shared
        client.getPaymentMethods(completionHandler: { (paymentMethodDicts) in
            var paymentMethods = [PaymentMethod]()
            if paymentMethodDicts != nil {
                for dict in paymentMethodDicts! {
                    let paymentMethod = PaymentMethod(id: dict["id"] as? String, name: dict["name"] as? String, type: dict["payment_type_id"] as? String, status: dict["status"] as? String, imageURL: dict["thumbnail"] as? String, minAllowedAmount: dict["min_allowed_amount"] as? Double, maxAllowedAmount: dict["max_allowed_amount"] as? Double)
                    
                    paymentMethods.append(paymentMethod)
                }
            }
            success(paymentMethods)
        }, errorHandler: { (error) in
            fail(error!)
        })
    }

}
