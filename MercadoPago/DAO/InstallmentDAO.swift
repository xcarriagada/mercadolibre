//
//  InstallmentDAO.swift
//  MercadoPago
//
//  Created by Ximena Arriagada on 19-10-18.
//

import Foundation

class InstallmentDAO {
    
    func installments(for amount: Double,_ paymentMethodId: String,_ cardIssuerId: String? = nil, onFailure fail: @escaping ((String?) -> ()), onSuccess success: @escaping ([Installment]?) -> ()) {
        var installments = [Installment]()
        let client = APIClient.shared
        if cardIssuerId != nil {
            client.getInstallments(for: paymentMethodId, amount, cardIssuerId, completionHandler: { (installmentsDicts) in
                if installmentsDicts != nil {
                    for dict in installmentsDicts! {
                        let installment = Installment(message: dict["recommended_message"] as? String)
                        installments.append(installment)
                    }
                }
                success(installments)
            }, errorHandler: { (error) in
                fail(error!)
            })
        } else {
            client.getInstallments(for: paymentMethodId, amount, completionHandler: { (installmentsDicts) in
                if installmentsDicts != nil {
                    for dict in installmentsDicts! {
                        let installment = Installment(message: dict["recommended_message"] as? String)
                        installments.append(installment)
                    }
                }
                success(installments)
            }, errorHandler: { (error) in
                fail(error!)
            })
        }
    }
    
}
