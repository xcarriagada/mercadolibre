//
//  DetailTableViewCell.swift
//  MercadoPago
//
//  Created by Ximena Arriagada on 20-10-18.
//

import UIKit
import Kingfisher

class DetailTableViewCell: UITableViewCell {

    @IBOutlet weak var detailView: UIView!
    @IBOutlet weak var detailWidthLayoutConstraint: NSLayoutConstraint!
    @IBOutlet weak var detailImageView: UIImageView!
    @IBOutlet weak var detailLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }
    
    func loadCreditCard(viewModel: CreditCardViewModel) {
        
        self.detailView.isHidden = false
        self.detailView.layer.cornerRadius = 5
        
        if let name = viewModel.name {
            self.detailLabel.text = name
        }
        
        if let imageURL = viewModel.imageURL, let url = URL(string: imageURL) {
            self.detailImageView.kf.setImage(with: url)
        }
        
    }
    
    func loadCardIssuer(viewModel: CardIssuerViewModel) {
        self.detailView.isHidden = false
        
        if let cardIssuer = viewModel.cardIssuer, let name = cardIssuer.name {
            self.detailLabel.text = name
        }
        
        if let cardIssuer = viewModel.cardIssuer, let imageURL = cardIssuer.imageURL, let url = URL(string: imageURL) {
            self.detailImageView.kf.setImage(with: url)
        }
        
    }
    
    func loadInstallment(viewModel: InstallmentViewModel) {
        self.detailWidthLayoutConstraint.constant = 0
        
        if let message = viewModel.message {
            self.detailLabel.text = message
        }
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
    
}
